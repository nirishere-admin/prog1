<?php
require_once ("db.php");

$search = $_POST['search'];

$sql = "SELECT ID, CountryName, CapitalName, CapitalLatitude, CapitalLongitude, CountryCode, ContinentName FROM countries WHERE countryName like '%".$search."%'";

$query = $pdo_db->prepare($sql);
$res = $query->execute();
$results =  array();

try{
    if($res)
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
} catch(PDOException $e){
    echo "Bad DB Access";
    return;
}

$results = [];

foreach ($rows as $result){
    $results[] = array("value"=>$result['CapitalName'],"label"=>$result['CountryName']);
}

echo json_encode($results);

die();

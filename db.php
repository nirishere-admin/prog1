<?php
require_once ("config.php");

$pdo_dsn = "mysql:host=".DB_SERVER.";dbname=".DB_NAME.";charset=utf8";
$pdo_user = DB_USER;
$pdo_pass = DB_PASS;

//now set up connection to db with PDO
// 1. Create a database connection
try {
    $pdo_db = new PDO($pdo_dsn, $pdo_user, $pdo_pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
    $pdo_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    if (!$pdo_db) {
        die("DIE:Database PDO Internal error #7763: ");// . mysql_error());
    }
}
catch(PDOException $e)
{
    echo $e->getMessage();
}

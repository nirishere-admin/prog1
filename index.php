<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="CSS/main.css">
    <!-- Script -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- jQuery UI -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="JS/autocomplete.js"></script>
</head>
<body>

<div id="wrapper">
    <div id="container">
        <table>
            <tr>
                <td>
                    <div id="main_text">Capitals of the world</div>
                </td>
            </tr>
            <tr>
                <td><input type='text' placeholder="Country Name" id='autocomplete'></td>
                <td><input type='text' id='selected' placeholder="Capital" disabled></td>
            </tr>
        </table>
    </div>
</div>
</body>

</html>

